# Amiga QNX Wallpaper Remakes

These wallpapers and stock graphics are done with Gimp and Affinity Photo/Designer and are inspired by the [never released QNX for Amiga](http://www.bambi-amiga.co.uk/amigahistory/qnxpup.html).

| File | Description |
|---|---|
| ```Amiga QNX Wallpaper Remakes 1 - MorphOS (.psd/.afphoto)``` | PSD and Affinity Photo sources |
| ```Stock/Amiga QNX Wallpaper Remakes 1 bottom bar - landscape.png``` | bottom bar, overlay for your wallpapers |
| ```Stock/Morpho (.svg/.afdesign)``` | the morpho i created for the MorphOS wallpapers |
| ```Wallpaper/Amiga QNX Wallpaper Remakes 1 - landscape *.png``` | classic blue and checkered bottom bar wallpaper \*¹ |
| ```Wallpaper/Amiga QNX Wallpaper Remakes 1 - MorphOS *.png``` | MorphOS themed wallpapers in two background pattern variants \*² |


\*¹ i used stock paper images by [dierat](https://www.deviantart.com/dierat/art/Paper-Pack-9-269825102)
\*² i used stock textures ["Antiche cover book Seamless textures 3" by jojo-ojoj](https://www.deviantart.com/jojo-ojoj/art/Antiche-cover-book-Seamless-textures-3-520755428), ["Light Blue Construction Paper Texture"](https://www.photos-public-domain.com/2011/01/24/light-blue-construction-paper-texture/) and stock image ["Texture 6" by CastleGraphics](https://www.deviantart.com/castlegraphics/art/Texture-6-832312798)
